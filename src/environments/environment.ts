// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAMyw2mWKJ2nRruJEkLZBtZX3nZ8zlJoTE',
  authDomain: 'tested-fb283.firebaseapp.com',
  databaseURL: 'https://tested-fb283.firebaseio.com',
  projectId: 'tested-fb283',
  storageBucket: 'tested-fb283.appspot.com',
  messagingSenderId: '797256935943',
  appId: '1:797256935943:web:9d5f8c42d242c1522a18a2',
  measurementId: 'G-YSRZG3RWRE'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
