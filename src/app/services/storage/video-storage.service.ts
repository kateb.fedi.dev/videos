import { Injectable } from '@angular/core';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideoStorageService {
  uid: string;
  constructor(private readonly storage: AngularFireStorage) {
    this.uid = localStorage.getItem('uid');
  }

  uploadFileAndGetMetadata(
    fileToUpload: File,
  ) {
    const { name } = fileToUpload;
    const filePath = `videos/${this.uid}/${new Date().getTime()}_${name}`;
    const uploadTask: AngularFireUploadTask = this.storage.upload(
      filePath,
      fileToUpload,
    );
    return {
      uploadProgress$: uploadTask.percentageChanges(),
      downloadUrl$: this.getDownloadUrl$(uploadTask, filePath),
    };
  }

  private getDownloadUrl$(
    uploadTask: AngularFireUploadTask,
    path: string,
  ): Observable<string> {
    return from(uploadTask).pipe(
      switchMap((_) => this.storage.ref(path).getDownloadURL()),
    );
  }


  getFileUploads() {
    const filesPath = `videos/${this.uid}`;
    const listRef = this.storage.ref(filesPath);
    return listRef;
  }

  getFile(video) {
    const listRef = this.storage.ref(video).getDownloadURL();
    return listRef;
  }
}
