import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  resultRAW: any;
  private resultObservable: Observable<User[]>;
  constructor(private afs: AngularFirestore, private http: HttpClient) { }

  save(user: User): Promise<DocumentReference> {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('user').doc(user.uid).set({
        email: user.email,
        uid: user.uid,
        firstname: user.firstname,
        lastname: user.lastname,
      });
    });
  }


  getUsers() {
    return this.afs.collection('user').doc('user').get();}

}
