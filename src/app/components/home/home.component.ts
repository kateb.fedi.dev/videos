import { Component, OnInit } from '@angular/core';
import { VideoStorageService } from '../../services/storage/video-storage.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  file: File;
  uploadProgress: any;
  downloadUrl: any;
  video: any;
  listofvideos: any;
  constructor(public videoStorageService: VideoStorageService) { }

  ngOnInit() {
    this.videoStorageService.getFileUploads().listAll().subscribe(
      res => {
        this.listofvideos = res.items;
        /*
        // here i am trying to get the src of iframe
              this.video= res.items[0].getDownloadURL()
              this.video = this.videoStorageService.getFile(this.video)
        */
      }
    );
  }


  onChange(event) {
    this.file = event.srcElement.files[0];
    console.log(this.file);

  }

  upload() {
    const object = this.videoStorageService.uploadFileAndGetMetadata(this.file);
    object.downloadUrl$.subscribe(res => console.log(res));

  }


}
