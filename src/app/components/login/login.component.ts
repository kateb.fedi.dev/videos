import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService } from '../../services/auth/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  firstname: string;
  lastname: string;
  constructor(public auth: AuthService, public router: Router, public userService: UserService) { }

  ngOnInit() {
  }

  googleSignin() {
    this.auth.googleSignin()
      .then(response => console.log(response))
      .catch(err => console.log(err));
  }


  add(Email, Uid) {
    this.user = {
      uid: Uid,
      email: Email,
      firstname: this.firstname,
      lastname: this.lastname
    };
    if (this.firstname !== undefined && this.lastname !== undefined) {
      this.userService.save(this.user);
    }
    localStorage.setItem('uid', Uid);
    this.router.navigate(['/home']);
  }

  gotohome(Uid) {
    localStorage.setItem('uid', Uid);
    this.router.navigate(['/home']);
  }

}


